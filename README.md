# Audiobookshelf Docker

[![audiobookshelf][audiobookshelf-version-src]][audiobookshelf-version-href]

Self-hosted audiobook and podcast server.

- [GitHub](https://github.com/advplyr/audiobookshelf)
- [Documentation](https://www.audiobookshelf.org/docs)
- [Audiobookshelf (android)](https://play.google.com/store/apps/details?id=com.audiobookshelf.app)
- [Docker hub](https://hub.docker.com/r/advplyr/audiobookshelf/tags)

## Docker

> Tested for Audiobookshelf 2.15.1

Create a `.env` file from `.env.example` and change the values.

```bash
cp .env.example .env
```

- `CONFIG_PATH` is local path to audiobookshelf config
- `METADATA_PATH` is local path to audiobookshelf metadata
- `TIMEZONE` is `UTC` by default
- `CONTAINER_RESTART`: restart policy for the container (`no`, `always`, `unless-stopped`, `on-failure`, `always`)
- `APP_PORT` is `13378` by default
- `APP_VERSION` is `latest` by default (you can use any version from [releases](https://hub.docker.com/r/advplyr/audiobookshelf/tags))

You have five variables to set the path to the media files:

> If you don't use a specific media type, remove it from `.env` file and from `docker-compose.yml` file.

- `AUDIO_PATH` for audio content
- `BOOKS_PATH` for books content

Build and run the docker compose

```bash
docker compose down -v
docker compose up -d --remove-orphans
```

Audiobookshelf is now running on <http://localhost:13378> (default port).

You can use [this template](./nginx/nginx.conf) to host with NGINX, adapt to your needs.

## Options

### Bash

Execute bash in the app container

```bash
docker container exec -it audiobookshelf /bin/sh
```

### Logs

Check logs

```bash
docker logs audiobookshelf # docker logs audiobookshelf -f for live logs
```

### Update

Update audiobookshelf

```bash
docker compose down -v
docker compose pull
docker compose up -d --remove-orphans
```

[audiobookshelf-version-src]: https://img.shields.io/static/v1?style=flat&label=audiobookshelf&message=2.15.1&color=cd9d49&logo=audiobookshelf&logoColor=ffffff&labelColor=18181b
[audiobookshelf-version-href]: https://www.audiobookshelf.com/docs
